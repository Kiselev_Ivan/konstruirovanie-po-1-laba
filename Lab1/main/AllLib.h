#ifndef _ALL_
#define _ALL_

#include "../Chanel/ChanelFactory.h"
#include "../Reciver/ReciverFactory.h"
#include "../SenderFactory/SenderFactory.h"
#include "../SignalFactory/SignalFactory.h"

#include "ADConverter.h"
#include "DAConverter.h"

#endif // !_ALL_
