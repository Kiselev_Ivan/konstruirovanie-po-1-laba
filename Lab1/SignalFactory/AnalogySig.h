#ifndef _AS_
#define _AS_

#include "Signal.h"

class AnalogySignal : public Signal
{
public:

	AnalogySignal(int p=0)
	{
		value = p;
		cout << "AnalogySignal -> ";
	}

	~AnalogySignal() {};

};

#endif