#ifndef _DS_
#define _DS_

#include "Signal.h"

class DigitalSignal : public Signal
{
public:
	DigitalSignal(int p = 0)
	{
		value = p;
		cout << "DigitalSignal -> ";
	}

	~DigitalSignal() {};
}

#endif