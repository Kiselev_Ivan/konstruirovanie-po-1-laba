#ifndef _SF_
#define _SF_

#include "ISignalFactory.h"

class SignalFact : public ISigFact
{
public:
	static Signal* GetSignal(ISignal type)
	{
		switch (type)
		{
		case AnalogySig:
			return new AnalogySignal();
		case DigitalSig:
			return new DigitalSignal();
		default:
			return NULL;
		}
	}
};
#endif // !_SF_

