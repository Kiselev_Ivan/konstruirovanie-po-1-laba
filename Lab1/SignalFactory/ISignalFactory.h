#ifndef _ISF_
#define _ISF_

#include "AnalogySig.h"
#include "DigitalSig.h"

; using namespace std;

enum ISignal {AnalogySig , DigitalSig};

class ISigFact
{
public:
	virtual Signal* GetSignal() = 0;
};


#endif // !_ISF_
