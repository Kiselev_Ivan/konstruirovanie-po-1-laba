#ifndef _VChannel_
#define _VChannel_

#include <iostream>

using namespace std;

class VChannel
{
public:
	virtual void TakeAndTransmit() = 0;
};



#endif // !_Channel_

