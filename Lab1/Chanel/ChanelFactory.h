#ifndef _CF_
#define _CF_

#include "IChanelFactory.h"


class ChanFact : public IChanelFactory
{
public:
	static VChannel* GetChanel(IChanel type)
	{
		switch (type)
		{
		case AnChan:
			return new AnChanel();
		case DigChan:
			return new DigChanel();
		default:
			return NULL;
		}
	}
};

#endif // !_CF_

