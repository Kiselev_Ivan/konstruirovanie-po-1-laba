#ifndef _ICF_
#define _ICF_

#include "AnalogyChanel.h"
#include "DigitalChanel.h"

enum IChanel {AnChan,DigChan};

class IChanelFactory
{
public:
	virtual VChannel* GetChanel() = 0;
};

#endif // !_ICF_

