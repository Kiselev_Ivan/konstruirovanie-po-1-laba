#ifndef _Settings_
#define _Settings_

#include "../main/AllLib.h"
#include <mutex>

int Enter()
{
	int type;
	cout << ", Analogy - 0 , Digital - 1" << endl;
	cin >> type;
	cout << endl;
	if ((type == 1) || (type == 0))
	{
		return type;
	}
	else
	{
		cout << endl << "Enter error, it will been Analogy device" << endl;
		return 0;
	}
}

class Settings
{
private:
	 Settings() { };
	 int Res;
	 int Chan;
	 int Send;
	 int Sig;
	 static Settings* _instance;
public:

	static Settings* Instance()
	{
		mutex creatingInstance;
		creatingInstance.lock();
		if (_instance == NULL)
		{
			_instance = new Settings();
		}
		creatingInstance.unlock();
		return _instance;		
	}
	~Settings() {};

	void SetSettings()
	{
		cout << "Enter signal's type";
		*GetTSignal() = Enter();
		cout << "Enter sender's type";
		*GetTSender() = Enter();
		cout << "Enter channel's type";
		*GetTChannel() = Enter();
		cout << "Enter reciver's type";
		*GetTReciver() = Enter();
	}

	int* GetTReciver();

	int* GetTSender();

	int* GetTChannel();

	int* GetTSignal();


};

    Settings* Settings::_instance = NULL;

	int* Settings::GetTReciver()
	{
		return &Res;
	}

	int* Settings::GetTSender()
	{
		return &Send;
	}

	int* Settings::GetTChannel()
	{
		return &Chan;
	}

	int* Settings::GetTSignal()
	{
		return &Sig;
	}

#endif