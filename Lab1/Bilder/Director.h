#ifndef _Direct_
#define _Direct_

#include "../main/AllLib.h"
#include "../Settings/Settings.h"
#include "SimpleBilder.h"
#include "HardBilder.h"

class Direct
{
protected: 
	Settings* _setting = Settings::Instance();
public :
	Direct() { };
	~Direct() {};
	void Construct();
	Settings* GetSet();
	void GetSchem();

};

void Direct::Construct()
{
	_setting->SetSettings();
}

Settings* Direct::GetSet()
{
	return _setting;
}

void Direct::GetSchem()
{
	if ((*_setting->GetTSignal() == *_setting->GetTSender()) && (*_setting->GetTSender() == *_setting->GetTChannel()) && (*_setting->GetTReciver() == *_setting->GetTChannel()))
		SimpleBilder::GetSchem(_setting);
	else HardBilder::GetSchem(_setting);
}

#endif // !_Direct_
