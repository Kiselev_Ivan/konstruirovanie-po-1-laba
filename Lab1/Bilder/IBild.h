#ifndef _Bilder_
#define _Bilder_

#include "../Settings/Settings.h"

class IBilder
{
protected:
	virtual ISignal* BildSignal() = 0;
	virtual ISend* BildSender() = 0;
	virtual IChanel* BildChannel() = 0;
	virtual IReciver* BildReciver() = 0;
public:
	virtual void GetSchem() = 0;
};
#endif // !_Bilder_

