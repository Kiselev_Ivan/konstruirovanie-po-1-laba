#ifndef _SBilder_
#define _SBilder_

#include "IBild.h"
#include "../main/AllLib.h"


class SimpleBilder : protected IBilder
{
protected:
	static Signal* BildSignal(Settings* _setting)
	{
		if (*_setting->GetTSignal()==1)
			return 	SignalFact::GetSignal(DigitalSig);
		else
			return SignalFact::GetSignal(AnalogySig);
	}

	static Sender* BildSender(Settings* _setting)
	{
		if (*_setting->GetTSender()==1)
			return 	SendFact::GetSender(DigSend);
		else
			return SendFact::GetSender(AnSend);
	}
	
	static VChannel* BildChannel(Settings* _setting)
	{
		if (*_setting->GetTChannel()==1)
			return 	ChanFact::GetChanel(DigChan);
		else
			return ChanFact::GetChanel(AnChan);
	}

	static IReciver* BildReciver(Settings* _setting)
	{
		if (*_setting->GetTReciver()==1)
			return 	Reciver::GetReciver(DigitalReciverType);
		else
			return Reciver::GetReciver(AnalogyReciverType);
	}
public:
	static void GetSchem(Settings* _setting)
	{

		BildSignal(_setting);
		
		BildSender(_setting)->SendSignal();
		
		BildChannel(_setting)->TakeAndTransmit();
		
		BildReciver(_setting)->GiveSignal();
		
	}
};

#endif // !_SBilder_
