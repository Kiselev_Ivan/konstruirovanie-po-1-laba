#ifndef _HBilder_
#define _HBilder_

#include "IBild.h"
#include "../main/AllLib.h"

static bool Not_Compatibility(int* first, int* second)
{
	if (*first != *second) return true;
	else return false;
}


class HardBilder : protected IBilder
{
protected:
	static Signal* BildSignal(Settings* _setting)
	{
		if (*_setting->GetTSignal()==1)
			return 	SignalFact::GetSignal(DigitalSig);
		else
			return SignalFact::GetSignal(AnalogySig);
	}

	static Sender* BildSender(Settings* _setting)
	{
		if (*_setting->GetTSender()==1)
			return 	SendFact::GetSender(DigSend);
		else
			return SendFact::GetSender(AnSend);
	}

	static VChannel* BildChannel(Settings* _setting)
	{
		if (*_setting->GetTChannel()==1)
			return 	ChanFact::GetChanel(DigChan);
		else
			return ChanFact::GetChanel(AnChan);
	}

	static IReciver* BildReciver(Settings* _setting)
	{
		if (*_setting->GetTReciver()==1)
			return 	Reciver::GetReciver(DigitalReciverType);
		else
			return Reciver::GetReciver(AnalogyReciverType);
	}
public:
	static void GetSchem(Settings* _setting)
	{
		BildSignal(_setting);
		if (Not_Compatibility(_setting->GetTSignal(), _setting->GetTSender()))
			if ((*_setting->GetTSignal()) == 1) DAConvert* con1 = new DAConvert();
			else ADConvert* con1 = new ADConvert();
			BildSender(_setting)->SendSignal();
		if (Not_Compatibility(_setting->GetTSender(), _setting->GetTChannel()))
			if (*_setting->GetTSender() == 1) DAConvert* con2 = new DAConvert();
			else ADConvert* con2 = new ADConvert();
			BildChannel(_setting)->TakeAndTransmit();
		if (Not_Compatibility(_setting->GetTChannel(), _setting->GetTReciver()))
			if (*_setting->GetTChannel() == 1) DAConvert* con3 = new DAConvert();
			else ADConvert* con3 = new ADConvert();
			BildReciver(_setting)->GiveSignal();
	}
};

#endif // !_SBilder_

