#ifndef _ISendF_
#define _ISendF_

#include "AnalogySender.h"
#include "DigitalSender.h"

enum ISend {AnSend,DigSend};

class ISendFact
{
public:
	virtual Sender* GetSender() = 0;
};

#endif // !_ISendF_

