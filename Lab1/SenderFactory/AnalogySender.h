#ifndef _ASend_
#define _ASend_

#include "Sender.h"

class ASend : public Sender
{
public:
	ASend() {};
	~ASend() {};
	
	void SendSignal()
	{
		cout << "Analogy Sender ->";
	}
};

#endif // !_DSend_
