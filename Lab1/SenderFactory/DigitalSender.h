#ifndef _DSend_
#define _DSend_

#include "Sender.h"

class DSend : public Sender
{
public :
	DSend() {};
	~DSend() {};
	
	void SendSignal()
	{
		cout << "Digital Sender ->";
	}
};

#endif // !_DSend_

