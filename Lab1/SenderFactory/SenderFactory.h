#ifndef _SendF_
#define _SendF_

#include "ISenderFactory.h"

class SendFact : public ISendFact
{
public:
	static Sender* GetSender(ISend type)
	{
		switch (type)
		{
		case AnSend:
			return new ASend();
		case DigSend:
			return new DSend();
		default:
			return NULL;
		}
	}
};
#endif // !_SendF_

