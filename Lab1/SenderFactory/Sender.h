#ifndef _Send_
#define _Send_

#include <iostream>

using namespace std;

class Sender
{
public:
	virtual void SendSignal() = 0;
};

#endif // !_Send_
