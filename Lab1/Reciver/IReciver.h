#ifndef _IRC_
#define _IRC_

class IReciver
{
public:
	virtual void GiveSignal() = 0;
};

#endif // !_IRC_
