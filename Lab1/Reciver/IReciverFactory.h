#ifndef _IRF_
#define _IRF_

#include "DigitalReciver.h"
#include "AnalogyReciver.h"

using namespace std;

enum ReciverType { AnalogyReciverType, DigitalReciverType };

class IReciverFactory
{
	virtual IReciver* GetReciver() = 0;
};

#endif // !_IRF_

