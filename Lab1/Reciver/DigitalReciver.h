#ifndef _DRec_
#define _DRec_

#include "IReciver.h" 

#include <iostream>

using namespace std;

class DigitalReciver: IReciver
{
	public:
		DigitalReciver() {};
		
		void GiveSignal()
		{
			cout << "Received Digital Signal" << endl;
		}
		~DigitalReciver() {};
};

#endif

