#ifndef _ARec_
#define _ARec_

#include "IReciver.h" 
#include <iostream>

using namespace std;

class AnalogyReciver: IReciver
{
	public:
	AnalogyReciver() {};

	void GiveSignal()
	{
		cout << "Received Analogy Signal" << endl;
	}
	~AnalogyReciver() {};
};


#endif // !_ARec_