#ifndef _Reciver_
#define _Reciver_

#include "IReciverFactory.h"

class Reciver : public IReciverFactory
{
public:
	static IReciver* GetReciver(ReciverType type)
	{
		switch (type)
		{
		case AnalogyReciverType:
			return (IReciver*)(new AnalogyReciver());
		case DigitalReciverType:
			return (IReciver*)(new DigitalReciver());
		default:
			return NULL;
		}
	}
};

#endif 



