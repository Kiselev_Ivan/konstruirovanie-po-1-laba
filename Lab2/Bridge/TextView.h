#pragma once
#include "IDecorator.h"
class TextView : public IDecorator {
protected:
	IDecorator* Next;
public:
	
	TextView(IDecorator* nextElem) {
		Next = nextElem;
	}
	void Draw() {
		Next->Draw();
		cout << "| _______________________THIS IS TEXT_______ |" << endl;
		cout << "| _______________THIS IS TEXT_______________ |" << endl;
		cout << "| _______THIS IS TEXT_______________________ |" << endl;
	}
};
