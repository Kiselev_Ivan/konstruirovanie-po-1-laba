#pragma once
#include "IDecorator.h"
class HorisontalSCroll : public IDecorator {
protected:
	IDecorator* Next;
public:
	
	HorisontalSCroll(IDecorator* nextElem) {
		Next = nextElem;
	}
	void Draw() {
		Next->Draw();
		cout << "|  **                                        |" << endl;
		cout << "| /|\\                                        |" << endl;
		cout << "|  | It is horisontal SCroll                 |" << endl;
	}
};

