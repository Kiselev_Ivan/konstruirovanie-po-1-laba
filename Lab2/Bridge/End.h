#pragma once
#include "IDecorator.h"

class StartWindow : public IDecorator {
public:
	StartWindow() {}
	void Draw() {
		cout << "|                                            |" << endl;
	}
};
