#pragma once
#include "IDecorator.h"

class VerticalSCroll : public IDecorator {
protected:
	IDecorator* Next;
public:
	VerticalSCroll(IDecorator* nextElem) {
		Next = nextElem;
	}
	void Draw() {
		Next->Draw();
		cout << "|                                            |*|	<- this is scroll" << endl;
		cout << "|                                            |*|	" << endl;
	}
};
