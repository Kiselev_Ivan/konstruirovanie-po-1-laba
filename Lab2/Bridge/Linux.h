#pragma once
#include "OS.h"

class LinuxOS : public OS {
public:
	LinuxOS() {}
	void DrawWindow() {
		Content = new StartWindow();
		IsWind = true;
	}
	void DrawVerticalScrool() {
		if (IsWind) {
			time = Content;
			Content = new VerticalSCroll(time);
		}
		else cout << "At first create Window";
	}
	void DrawHorizontalScrool() {
		if (IsWind) {
			time = Content;
			Content = new HorisontalSCroll(time);
		}
		else cout << "At first create Window";
	}
	void DrawTextBox() {
		if (IsWind) {
			time = Content;
			Content = new TextView(time);
		}
		else cout << "At first create Window";
	}
	void DrawImage() {
		if (IsWind) {
			time = Content;
			Content = new ProxyImage(time);
		}
		else cout << "At first create Window";

	}
	void DrawThisWindow() {
		if (IsWind) {
			cout << "----------------L-I-N-U-X------------------" << endl;
			OS::DrawThisWindow();
		}
		else cout << "At first create Window";
	}
};