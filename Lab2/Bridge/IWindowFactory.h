#pragma once
#include "Windows.h"
#include "IOS.h"
#include "Linux.h"
enum OpSy { Windows, Ios, Linux };

class IWindowFactory {
public:
	static OS * createWindow(OpSy  system = Windows) {
		switch (system) {
		case Windows: return new WindowsOS();
		case Ios: return new IOS();
		case Linux: return new LinuxOS();
		default: return new WindowsOS();
		}
	}
};

