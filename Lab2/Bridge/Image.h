#pragma once
#include "IDecorator.h"
class Image : public IDecorator {
public :
	void Draw() {
		cout << "| ######  I M A G E  ####################### |" << endl;
		cout << "| ################ I M A G E ############### |" << endl;
		cout << "| ########################  I M A G E  ##### |" << endl;
	}
	Image() {}
	~Image() {}
};
