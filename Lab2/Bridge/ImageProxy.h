#pragma once

#include "IDecorator.h"
#include "Image.h"
class ProxyImage : public IDecorator {
private: 
	IDecorator* image;
protected:
	IDecorator* Next;
public:
	ProxyImage(IDecorator* nextElem) {
		image = new Image();
		Next = nextElem;

	}
	~ProxyImage() {}

	void Draw() {
		Next->Draw();
		cout << "               used Proxy" << endl;
		image->Draw();
	}
};
