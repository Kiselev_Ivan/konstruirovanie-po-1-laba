#pragma once

#include "IWindowFactory.h"

class AboutWindow {
protected:
	OpSy WinType;
	OS* OSyst;

public:
	AboutWindow() {
		int type;
		cout << "Enter your operating system : 0 - Windows , 1 - IOS , 2 - Linux" << endl;
		cin >> type;
		cout << endl;
		switch (type) {
		case 0: { WinType = Windows; break; }
		case 1: { WinType = Ios; break; }
		case 2: { WinType = Linux; break;	}
		default: { WinType = Windows; break; }
		}
		OSyst = IWindowFactory::createWindow(WinType);
	}
	void DrawVerticalScroll() {
			OSyst->DrawVerticalScrool();
	}
	void DrawHorizontalScrool() {
			OSyst->DrawHorizontalScrool();
	}
	void DrawWindow() {
		OSyst->DrawWindow();
	}
	
	void DrawTextBox() {
			OSyst->DrawTextBox();
	}

	void DrawImage() {
		OSyst->DrawImage();
	}

	void Working() {
		int type = 0;
		while (type >= 0 && type <= 4)
		{
			cout << "What do you want to do" << endl;
			cout << "0 - create Window your OS;\n1 - create Vertical Scrool;\n2 - create Horizontal Scrool;\n3 - Add Text;\n4 - Add Image\n5 - Exit and Draw our window" << endl << endl;
			cin >> type;
			cout << endl;
			switch (type) {
			case 0: {
				DrawWindow();
				break;
			}
			case 1: {
				DrawVerticalScroll();
				break;
			}
			case 2: {
				DrawHorizontalScrool();
				break;
			}
			case 3: {
				DrawTextBox();
				break;
			}
			case 4: {
				DrawImage();
				break;
			}
			default:
				break;
			}
			cout << endl;
		}
	}

	void DrawThisWindow() {
		OSyst->DrawThisWindow();
	}

};
