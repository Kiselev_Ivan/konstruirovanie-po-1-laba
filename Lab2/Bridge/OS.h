#pragma once
#include <iostream>
#include "stdafx.h"
#include "ImageProxy.h"
#include "Horisontal.h"
#include "SCroll.h"
#include "TextView.h"
#include "End.h"

using namespace std;

class OS {
public:
	virtual void DrawWindow() = 0;
	virtual void DrawVerticalScrool() = 0;
	virtual void DrawHorizontalScrool() = 0;
	virtual void DrawTextBox() = 0;
	virtual void DrawThisWindow() {
		if (IsWind == true) {
			cout << "|                                            |" << endl;
			Content->Draw();
			cout << "|                                            |" << endl;
			cout << " _____________________________________________" << endl;
		}
	}
	virtual void DrawImage() = 0;

protected:
	IDecorator* Content;
	IDecorator* time;
	bool IsWind = false;
};