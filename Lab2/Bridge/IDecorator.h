#pragma once

#include <iostream>
using namespace std;
class IDecorator {
public:
	virtual void Draw() = 0;
};