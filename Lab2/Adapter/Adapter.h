#include "ExternalLog.h"

#ifndef _Adapt_
#define _Adapt_

class Adapter
{
private:
	ExternalLog* loger;
public:
	Adapter() { loger = new ExternalLog(); };
	~Adapter() {};

	void AdapterError(string str) {
		cout << "\a";
		loger->Log(str, Error);
		cout << "\a";		
	}
	void AdapterWarning(string str) {
		loger->Log(str, Warning);
	}
	void AdapterInfo(string str) {
		loger->Log(str, Info);
		
	}
};

#endif // !_Adapt_


