#include <string>
#include <iostream>

using namespace std;

#ifndef _ELog_
#define _ELog_

enum LogLevel {Error , Warning , Info};

class ExternalLog
{
public :
	void Log(string str, LogLevel LL);
};

void ExternalLog::Log(string str, LogLevel LL)
{
	switch (LL)
	{
	case Error:
		cout << endl << "ERROR : " << str << endl;
		break;
	case Warning:
		cout << endl << "Warning : " << str << endl;
		break;
	case Info:
		cout << endl << str << endl;
		break;
	default:
		break;
	}
}


#endif