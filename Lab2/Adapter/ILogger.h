#include "Adapter.h"

#ifndef _ILog_
#define _ILog_

class ILog
{
private:
	Adapter* adapt;
public:
	ILog() {
		adapt = new Adapter();
	}
	~ILog() {};
	void LogError(string str);
	void LogWarning(string str);
	void LogInfo(string str);
};

void ILog::LogError(string str)
{
	adapt->AdapterError(str);
}

void ILog::LogWarning(string str)
{
	adapt->AdapterWarning(str);
}

void ILog::LogInfo(string str)
{
	adapt->AdapterInfo(str);
}
#endif // !_ILog_
